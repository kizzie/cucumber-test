#Cucumber Test#

This repository is a simple cucumber example generated using Maven and Eclipse. The feature file is in the test/resources folder and the step file generated from running that feature via "mvn test"

Feature example used from: https://github.com/cucumber/cucumber/wiki/Feature-Introduction